# Shared rulesets

This repository holds ruleset configuration files that can be shared with other projects.

See [documentation](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html#specify-a-remote-configuration-file).
